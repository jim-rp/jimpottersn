package jim.potter.sn;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;

import org.junit.Test;

public class MD5ServiceImplTest {

	MD5ServiceImpl svc = new MD5ServiceImpl ();

	@Test 
	public void testGetChecksumfromFile () {
		
	}

	@Test
	public void testGetChecksum () throws NoSuchAlgorithmException {
		
		byte [] content = new String ("hello world\n").getBytes();
		String expectedMD5 = "6f5902ac237024bdd0c176cb93063dc4";

		String md5 = svc.getChecksum(content);
		
		assertEquals (expectedMD5, md5);
	}
	
}
