package jim.potter.sn;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class MD5ServiceImpl implements MD5Service {

	@Override
	public String getChecksumFromFile(MultipartFile p) {
		try {
			return getChecksum (p.getBytes());
		}
		catch (Exception e) {
			return "failed to calculate checksum - " + e.getMessage();
		}
	}
	
	String getChecksum (byte [] content) throws NoSuchAlgorithmException {

		MessageDigest md = MessageDigest.getInstance("MD5");
		return Hex.encodeHexString(md.digest(content));		
	}
}
