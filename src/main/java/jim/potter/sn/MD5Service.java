package jim.potter.sn;

import org.springframework.web.multipart.MultipartFile;

public interface MD5Service {

	String getChecksumFromFile (MultipartFile p);
}
