package jim.potter.sn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MD5Controller {

	@Autowired
	private MD5Service svc;
	
	@PostMapping ("/")
	public String getMD5 (@RequestParam(value="file") Object obj) {
		if (obj != null && obj instanceof MultipartFile) {
			return svc.getChecksumFromFile((MultipartFile)obj);
		}
		return "no file recieved";
	}
}
